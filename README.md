Task 3 - Simple eShop
========================

Requirements
--------------

1. PHP 7
2. MySQL 5.6

Setup
--------------

1. Clone repository
2. Go to root dir and run `composer install`
3. Create database by running `php bin\console doctrine:schema:update --force`
4. Insert default data with `php bin\console doctrine:migrations:migrate`
5. Run local server `php bin\console server:run`
6. Refresh homepage few times for pictures to generate cache

Users
--------------

There are 2 users by default:


- `testuser:seohelis`
- `batman:seohelis`

Extra stuff used:

  
  * [**LiipImagineBundle**][1] - Image manager

  * [**DoctrineMigrationsBundle**][2] - Bundle for migrations
  
  * [**Admin LTE**][3] - Bootstrap theme for front-end

[1]:  http://symfony.com/doc/current/bundles/LiipImagineBundle/index.html
[2]:  http://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html
[3]:  https://almsaeedstudio.com/themes/AdminLTE/index2.html


<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cart
 */
class Cart
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var array
     */
    private $products = [];

    /**
     * @var int
     */
    private $count = 0;
    
    /**
     * @var float
     */
    private $totalPrice = 0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set products
     *
     * @param array $products
     *
     * @return Cart
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }
    
    /**
     * @param Product $product
     * @return Cart
     */
    public function addProduct(Product $product)
    {
        // do not add the sime product
        if (in_array($product->getId(), $this->products)) {
            return $this;
        }
        
        $this->products[] = $product->getId();
        $this->totalPrice += $product->getPrice();
        $this->count++;
        
        return $this;
    }

    /**
     * Get products
     *
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return Cart
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
    
    /**
     * @return float
     */
    public function getTotalPrice() : float
    {
        return $this->totalPrice;
    }

    /**
     * @param mixed $totalPrice
     * @return Cart
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = floatval($totalPrice);
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function serializeForCookie() : string
    {
        $cart['products'] = $this->products;
        $cart['total'] = $this->getTotalPrice();
        $cart['count'] = $this->count;
        $cart['id'] = $this->id;
        
        return json_encode($cart);
    }
}

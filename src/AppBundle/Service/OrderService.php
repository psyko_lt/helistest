<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Product;
use AppBundle\Entity\SaleOrder;
use AppBundle\Entity\User;
use AppBundle\Repository\SaleOrderRepository;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Order service
 */
class OrderService
{
    /**
     * @var EntityManager
     */
    private $em;
    
    /**
     * @var SaleOrderRepository
     */
    private $orderRepo;
    
    /**
     * @param EntityManager $em
     * @param SaleOrderRepository $orderRepository
     */
    public function __construct(EntityManager $em, SaleOrderRepository $orderRepository)
    {
        $this->em = $em;
        $this->orderRepo = $orderRepository;
    }
    
    /**
     * @param User $user
     * @param array $products
     * @return bool
     */
    public function saveOrders(User $user, array $products) : bool
    {
        $tempOrders = $this->groupOrderItemsBySeller($products);
        
        if (empty($tempOrders)) {
            return false;
        }
        
        foreach ($tempOrders as $orderGroup) {
            $order = new SaleOrder();
            $order->setBuyer($user);
            $order->setSeller($orderGroup['user']);
            $order->setProducts(new ArrayCollection($orderGroup['products']));
            
            $order->setTotal($this->calcualteTotal($orderGroup['products']));
            $this->em->persist($order);
        }
        
        $this->em->flush();
        
        return true;
    }

    /**
     * @param SaleOrder $order
     * @param Product[] $items
     * @return mixed
     */
    public function calcualteTotal(array $items)
    {
        $total = 0;
        
        foreach ($items as $product) {
            $total += $product->getPrice();
            
            // decrease product stock
            $stock = $product->getStock();
            $product->setStock(--$stock);
            
            $this->em->persist($product);
        }
        
        return $total;
    }
    
    /**
     * @param int $userId
     * @return SaleOrder[]
     */
    public function getUserOrders(int $userId) : array
    {
        return $this->orderRepo->findBy(['seller' => $userId]);
    }
    
    /**
     * @param array $products
     * @return array
     */
    public function groupOrderItemsBySeller(array $products) : array
    {
        $grouped = [];
        
        foreach ($products as $product) {
            $grouped[$product->getUser()->getId()]['user'] = $product->getUser();
            $grouped[$product->getUser()->getId()]['products'][] = $product;
        }
        
        return $grouped;
    }
}

<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * File upload service
 */
class FileUploadService
{
    /**
     * @var string
     */
    private $targetDir;

    /**
     * @param string $targetDir Target directory for uploaded files
     */
    public function __construct(string $targetDir)
    {
        $this->targetDir = $targetDir;
    }

    /**
     * @param UploadedFile $file
     * @return string Saved filename
     */
    public function upload(UploadedFile $file) : string
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->targetDir, $fileName);

        return $fileName;
    }
}

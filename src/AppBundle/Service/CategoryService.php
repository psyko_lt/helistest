<?php

namespace AppBundle\Service;

use AppBundle\Entity\Category;
use AppBundle\Repository\CategoryRepository;

/**
 * Category service
 */
class CategoryService
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepo;
    
    /**
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepo = $categoryRepository;
    }
    
    /**
     * @return Category[]
     */
    public function getCategories() : array
    {
        return $this->categoryRepo->findBy([], ['name' => 'ASC']);
    }
}

<?php

namespace AppBundle\Service;

use AppBundle\Entity\Cart;
use AppBundle\Entity\Product;

/**
 * Category service
 */
class CartService
{
    /**
     * @param Product $product
     * @param string $cookie
     * @return Cart
     */
    public function addToCart(Product $product, string $cookie) : Cart
    {
        $cart = $this->getCart($cookie);
        $cart->addProduct($product);
        
        return $cart;
    }

    /**
     * Parses cookie into the Cart object
     * @param string $cookie
     * @return Cart
     */
    public function getCart(string $cookie) : Cart
    {
        $cartData = json_decode($cookie, true);
        
        $cart = new Cart();
        
        if (!empty($cartData['products'])) {
            $cart->setProducts($cartData['products']);
        }
        
        if (!empty($cartData['total'])) {
            $cart->setTotalPrice($cartData['total']);
        }
        
        if (!empty($cartData['count'])) {
            $cart->setCount($cartData['count']);
        }
        
        return $cart;
    }
    
    /**
     * Removes items from cart which belong to the same user
     * @param Cart $cart
     * @param int $userId
     * @return Cart
     */
    public function recalculateCart(Cart $cart, int $userId, $products)
    {
        $newCart = new Cart();
        
        foreach ($products as $product) {
            if ($product->getUser()->getId() === $userId) {
                continue;
            }
            
            $newCart->addProduct($product);
        }
        
        return $newCart;
    }
}

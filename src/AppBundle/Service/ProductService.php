<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Product;
use AppBundle\Repository\ProductRepository;
use Doctrine\Common\Collections\Collection;

/**
 * Product service
 */
class ProductService
{
    /**
     * @var EntityManager
     */
    private $em;
    
    /**
     * @var ProductRepository
     */
    private $productRepo;
    
    /**
     * @param EntityManager $em
     * @param ProductRepository $productRepository
     */
    public function __construct(EntityManager $em, ProductRepository $productRepository)
    {
        $this->em = $em;
        $this->productRepo = $productRepository;
    }

    /**
     * @param Product $product
     */
    public function saveProduct(Product $product)
    {
        $this->em->persist($product);
        $this->em->flush();
    }
    
    /**
     * @param type array $filters
     * @return Collection
     */
    public function getProducts(array $filters) : Collection
    {
        return $this->productRepo->getProducts($filters);
    }
    
    /**
     * @param int $userId
     * @return Product[]
     */
    public function getProductsByUser(int $userId) : array
    {
        return $this->productRepo->findBy(['user' => $userId]);
    }
    
    /**
     * @param int $id
     * @return Product
     */
    public function getProductById(int $id) : Product
    {
        return $this->productRepo->find($id);
    }
    
    /**
     * @param int $categoryId
     * @return Product[]
     */
    public function getProductsByCategory(int $categoryId) : array
    {
        return $this->productRepo->findBy(['category' => $categoryId]);
    }
    
    /**
     * @param array $ids
     * @return Product[]
     */
    public function getProductsByIds(array $ids) : array
    {
        return $this->productRepo->findBy(['id' => $ids]);
    }
}

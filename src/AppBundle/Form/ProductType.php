<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', null, [
                    'label' => 'forms.product.title',
                    'attr' => ['class' => 'form-control'],
                ])
                ->add('category', EntityType::class, [
                    'label' => 'forms.product.category',
                    'attr' => ['class' => 'form-control'],
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'name',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->orderBy('c.name', 'ASC');
                    },
                    ])
                ->add('picture', FileType::class, [
                    'label' => 'forms.product.picture',
                    ])
                                ->add('price', NumberType::class, [
                    'label' => 'forms.product.price',
                    'attr' => ['class' => 'form-control'],
                    ])
                ->add('description', null, [
                    'label' => 'forms.product.description',
                    'attr' => ['class' => 'form-control'],
                    ])
                ->add('stock', IntegerType::class, [
                    'label' => 'forms.product.stock',
                    'attr' => ['class' => 'form-control'],
                ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_product';
    }
}

<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * @Route("/orders")
 * @Security("has_role('ROLE_USER')")
 */
class OrderController extends BaseController
{
    /**
     * @Route("/checkout", name="checkout")
     * @param Request $request
     * @return Response
     */
    public function checkoutAction(Request $request)
    {
        $cartService = $this->getCartService();
        $cart = $cartService->getCart($this->getCookie($request));
        $items = $this->getProductService()->getProductsByIds($cart->getProducts());
        $cart = $cartService->recalculateCart($cart, $this->getUser()->getId(), $items);
        
        $expires = strtotime('+1 day');
        $response = new Response();
        $response->headers->setCookie(new Cookie($this->getParameter('cookieName'), $cart->serializeForCookie(), $expires));
        $response->headers->setCookie(new Cookie($this->getParameter('cookieNameTotal'), $cart->getTotalPrice(), $expires));
        
        return $this->render(
            'order/checkout.html.twig',
            [
                'cart' => $cart,
            ],
            $response
        );
    }

    /**
     * @Route("/place", name="orders_place")
     * @param Request $request
     * @return RedirectResponse
     */
    public function placeOrderAction(Request $request)
    {
        $cart = $this->getCartService()->getCart($this->getCookie($request));
        $items = $this->getProductService()->getProductsByIds($cart->getProducts());
        
        $result = $this->getOrderService()->saveOrders($this->getUser(), $items);
        
        if ($result) {
            $this->addFlash('notice', 'texts.orderSuccess');
            
            return $this->redirectToRoute('orders_complete');
        }
        
        $this->addFlash('error', 'texts.orderFail');
        
        return $this->redirectToRoute('checkout');
    }
    
    /**
     * @Route("/list", name="orders_my")
     * @return Response
     */
    public function myOrdersAction()
    {
        return $this->render(
            'order/myOrders.html.twig',
            [
                'orders' => $this->getOrderService()->getUserOrders($this->getUser()->getId()),
            ]
        );
    }
    
    /**
     * @Route("/complete", name="orders_complete")
     * @param Request $request
     * @return Response
     */
    public function completeAction(Request $request)
    {
        $response = new Response();
        $response->headers->setCookie(new Cookie($this->getParameter('cookieName'), null, '-1'));
        $response->headers->setCookie(new Cookie($this->getParameter('cookieNameTotal'), null, '-1'));
        
        return $this->render(
            'order/complete.html.twig',
            [],
            $response
        );
    }
}

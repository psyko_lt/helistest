<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ShopController extends BaseController
{
    /**
     * @Route("/", name="homepage")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $categories = $this->getCategoryService()->getCategories();
        
        $filters = [];
        
        $order = $request->get('orderBy');
        $category = $request->get('category');
        
        if (!empty($order)) {
            $filters['order'] = $order;
        }
        
        if (!empty($category)) {
            foreach ($categories as $row) {
                if ($row->getId() === (int)$category) {
                    $filters['category'] = $row;
                    break;
                }
            }
        }
        
        return $this->render('shop/homepage.html.twig', [
            'products' => $this->getProductService()->getProducts($filters),
            'categories' => $categories,
        ]);
    }
    
    /**
     * @Route("/category/{id}/list", name="product_category")
     *
     * @param int $id
     * @return Response
     */
    public function categoryAction(int $id)
    {
        $products = $this->getProductService()->getProductsByCategory($id);
        $categories = $this->getCategoryService()->getCategories();
        
        $current = '';
        
        foreach ($categories as $cat) {
            if ($id === $cat->getId()) {
                $current = $cat->getName();
                break;
            }
        }
        
        return $this->render('shop/category.html.twig', [
            'products' => $products,
            'categories' => $categories,
            'currentCategory' => $current,
        ]);
    }
    
    /**
     * @Route("/cart", name="cart")
     *
     * @param Request $request
     * @return Response
     */
    public function cartAction(Request $request)
    {
        $cart = $this->getCartService()->getCart($this->getCookie($request));
        $items = $this->getProductService()->getProductsByIds($cart->getProducts());
        
        return $this->render('shop/cart.html.twig', [
            'products' => $items,
            'cart' => $cart,
        ]);
    }
    
    /**
     * @Route("/cart/add", name="add_to_cart")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addToCartAction(Request $request)
    {
        $productId = $request->get('id');
        
        if (!$productId) {
            return $this->json(['error' => 'No product ID']);
        }
        
        $cookieData = $this->getCookie($request);
        
        $product = $this->getProductService()->getProductById($productId);
        $cart = $this->getCartService()->addToCart($product, $cookieData);
        
        $expires = strtotime('+1 day');
        $response = new JsonResponse();
        $response->headers->setCookie(new Cookie($this->getParameter('cookieName'), $cart->serializeForCookie(), $expires));
        $response->headers->setCookie(new Cookie($this->getParameter('cookieNameTotal'), $cart->getTotalPrice(), $expires));
        
        $response->setData(['cartTotal' => $cart->getTotalPrice()]);
        
        return $response;
    }
}

<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\ProductType;
use AppBundle\Entity\Product;

/**
 * @Route("/products")
 */
class ProductController extends BaseController
{
    
    /**
     * @Route("/add", name="products_add")
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $product->getPicture();
            $uploader = $this->getFileUploaderService();
            $fileName = $uploader->upload($file);
                
            $product->setPicture($fileName);
            $product->setUser($this->getUser());
            
            $productService = $this->getProductService();
            $productService->saveProduct($product);

            $this->addFlash('success', 'messages.product.addSuccess');
            
            return $this->redirectToRoute('user_products');
        }
        
        return $this->render(
            'product/add.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route("/view/{id}", name="product_view")
     * @param int $id
     * @return Response
     */
    public function viewAction(int $id)
    {
        $product = $this->getProductService()->getProductById($id);
        
        return $this->render(
            'product/view.html.twig',
            [
                'product' => $product,
            ]
        );
    }
}

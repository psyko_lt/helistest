<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\UserPasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/user")
 * @Security("has_role('ROLE_USER')")
 */
class UserController extends BaseController
{
    
    /**
     * @Route("/profile", name="user_profile")
     * @param Request $request
     * @return Response
     */
    public function profileAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserPasswordType::class, $user);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            
            $userService = $this->getUserService();
            $userService->saveUser($user);

            $this->addFlash('success', 'messages.password.success');
        }
        
        return $this->render(
            'user/profile.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route("/products", name="user_products")
     */
    public function productsAction()
    {
        $products = $this->getProductService()->getProductsByUser($this->getUser()->getId());
        
        return $this->render(
            'user/products.html.twig',
            [
                'products' => $products,
            ]
        );
    }
}

<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\ProductService;
use AppBundle\Service\CategoryService;
use AppBundle\Service\CartService;
use AppBundle\Service\UserService;
use AppBundle\Service\FileUploadService;
use AppBundle\Service\OrderService;

/**
 * Extendable Base controller which contains most common methods
 */
class BaseController extends Controller
{
    /**
     * @param Request $request
     * @return string
     */
    public function getCookie(Request $request) : string
    {
        $cookieName = $this->getParameter('cookieName');
        $cookies = $request->cookies;
        
        return $cookies->has($cookieName) ? $cookies->get($cookieName) : '';
    }
    
    /**
     * @return ProductService
     */
    public function getProductService() : ProductService
    {
        return $this->get('app.product_service');
    }
    
    /**
     * @return CategoryService
     */
    public function getCategoryService() : CategoryService
    {
        return $this->get('app.category_service');
    }
    
    /**
     * @return CartService
     */
    public function getCartService() : CartService
    {
        return $this->get('app.cart_service');
    }
    
    /**
     * @return UserService
     */
    public function getUserService() : UserService
    {
        return $this->get('app.user_service');
    }
    
    /**
     * @return FileUploadService
     */
    public function getFileUploaderService() : FileUploadService
    {
        return $this->get('app.file_uploader_service');
    }
    
    /**
     * @return OrderService
     */
    public function getOrderService() : OrderService
    {
        return $this->get('app.order_service');
    }
}

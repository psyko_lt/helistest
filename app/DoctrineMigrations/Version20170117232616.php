<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170117232616 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $password = '$2y$13$XcI4xEIJnoe.o/pgqtLIiuwZW0pbZzk3EDjwqsVI8dfrAGK/3LJ5K'; // seohelis
        
        $queries = [
            "INSERT INTO `users` (`id`, `username`, `password`, `email`, `roles`) VALUES ('1', 'testuser', '$password', 'tester@test.com', '[]')",
            "INSERT INTO `users` (`id`, `username`, `password`, `email`, `roles`) VALUES ('2', 'batman', '$password', 'bat@man.com', '[]')",
            "INSERT INTO `category` (`id`, `name`) VALUES ('1', 'Electronics')",
            "INSERT INTO `category` (`id`, `name`) VALUES ('2', 'Home, Garden & Tools')",
            "INSERT INTO `category` (`id`, `name`) VALUES ('3', 'Beauty & Health')",
            "INSERT INTO `category` (`id`, `name`) VALUES ('4', 'Automotive')",
            "INSERT INTO `category` (`id`, `name`) VALUES ('5', 'Toys, Kids & Baby')",
            "INSERT INTO `products` (`id`, `title`, `price`, `description`, `picture`, `stock`, `user_id`, `category_id`) VALUES ('2', 'Old Car', '1000', 'This is kind of an old car. Take it or leave it.', '4586c48181b0b52c61eb39391d8184d8.jpeg', '1', '1', '4')",
            "INSERT INTO `products` (`id`, `title`, `price`, `description`, `picture`, `stock`, `user_id`, `category_id`) VALUES ('3', 'Green shirt', '10', 'Worn only once. Great deal.', '751558cb7a358e040895fb3406cd8461.jpeg', '1', '1', '3')",
            "INSERT INTO `products` (`id`, `title`, `price`, `description`, `picture`, `stock`, `user_id`, `category_id`) VALUES ('4', 'Iphone 8 - mint condition', '900', 'Brand new unreleased iPhone. Never seen before. Leaked from a factory in China. Be first to get to the future!', 'd62edb15a33585d8ebeea24475ea2857.jpeg', '3', '1', '1')",
            "INSERT INTO `products` (`id`, `title`, `price`, `description`, `picture`, `stock`, `user_id`, `category_id`) VALUES ('5', 'Brown sneakers', '25', 'Brown worn sneakers. No brand. Good condition.', 'cf8aad2dd3a363f95624307be083b5db.jpeg', '1', '1', '3')",
            "INSERT INTO `products` (`id`, `title`, `price`, `description`, `picture`, `stock`, `user_id`, `category_id`) VALUES ('6', 'Garden decoration - lion', '95', '4 garden decorations for sale. Good condition. Used for 2 years.', '1e48ef7cb430a560ffbe9ba29ef23637.jpeg', '2', '1', '2')",
            "INSERT INTO `products` (`id`, `title`, `price`, `description`, `picture`, `stock`, `user_id`, `category_id`) VALUES ('7', '4 Tires', '400', '4 tires for any SUV. Sold together. Slightly used but still in good condition.', 'd240585e048e557d5f3c330a9514888f.jpeg', '1', '2', '4')",
            "INSERT INTO `products` (`id`, `title`, `price`, `description`, `picture`, `stock`, `user_id`, `category_id`) VALUES ('8', 'Amazing Bag', '40', 'An amazing bag... Actually a simple bag.', 'd91f4cd6c7d458dd235629588cdf2306.jpeg', '2', '2', '3')",
        ];
        
        foreach ($queries as $sql) {
            $this->addSql($sql);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // Just use doctrine:schema:drop --force :)
    }
}

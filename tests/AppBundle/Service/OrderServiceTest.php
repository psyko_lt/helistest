<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\OrderService;
use Doctrine\ORM\EntityManager;
use AppBundle\Repository\SaleOrderRepository;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;

class OrderServiceTest extends \PHPUnit_Framework_TestCase
{
    private function getOrderServiceMock()
    {
        $entityManager = $this->createMock(EntityManager::class);
        $repo = $this->createMock(SaleOrderRepository::class);

        return new OrderService($entityManager, $repo);
    }
    
    /**
     * Test for calcualteTotal
     * @dataProvider productProvider
     */
    public function testCalcualteTotal($products, $total)
    {
        $mockService = $this->getOrderServiceMock();
        
        $this->assertEquals($total, $mockService->calcualteTotal($products));
    }
    
    /**
     * Test for groupOrderItemsBySeller
     * @dataProvider groupProvider
     */
    public function testGroupOrderItemsBySeller($products, $count)
    {
        $mockService = $this->getOrderServiceMock();
  
        $this->assertCount($count, $mockService->groupOrderItemsBySeller($products));
    }
    
    public function productProvider()
    {
        $prod1 = new Product();
        $prod1->setPrice(5);
        
        $prod2 = new Product();
        $prod2->setPrice(13);
        //-----------------------
        $prod3 = new Product();
        
        $prod4 = new Product();
        $prod4->setPrice(1);
        
        return [
            [[$prod1, $prod2], 18],
            [[$prod3, $prod4], 1],
            [[$prod3, $prod3], 0],
        ];
    }
    
    public function groupProvider()
    {
        $user1 = $this->createMock(User::class);
        $user1->method('getId')
             ->willReturn(1);
        
        $user2 = $this->createMock(User::class);
        $user2->method('getId')
             ->willReturn(2);
        //------------------------------
        
        $prod1 = (new Product())->setUser($user1);
        $prod2 = (new Product())->setUser($user1);
        
        $prod5 = (new Product())->setUser($user2);
        $prod6 = (new Product())->setUser($user2);
        
        return [
            [[$prod1, $prod2], 1],
            [[$prod1, $prod2, $prod5], 2],
            [[$prod1, $prod2, $prod5, $prod6], 2],
            [[], 0],
        ];
        
    }
}

